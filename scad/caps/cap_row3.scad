use <../src/lib/mx-keycap.scad>

// 7/10 feels good but is pretty high  (1)
// 6/7 looks good but is less comfortable – at least when not considering row 4  (2)
mx_cap_square(height=6, tilt=8);
