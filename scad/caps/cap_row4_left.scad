use <../src/lib/mx-keycap.scad>

mx_cap_square(height=8.5, tilt=13, length=20, y_offset=-1.25, width=20, x_offset=1.25, indent_x_offset=0.5);
