use <lib/cherrymx_switch.scad>
use <lib/plate_bolt.scad>
use <lib/promicro_mount.scad>
use <lib/screw_flat_head.scad>
use <lib/nut.scad>
use <lib/stripe.scad>
use <thumb_iface.scad>
include <../parameters.scad>


/*
 * Returning a 2D shape of the finger plate 
 */
module stand_cutaway() {
    translate([-screw_bolt_d/2, 0, case_bottom_z()])
        rotate(9.55,[0,1,0])
            color("Maroon") translate([0,-30,- 50]) cube([200, 150, 50]);
}

module finger_plate_outline(with_screw_bolts=true) {
    if (with_screw_bolts) {
        for (i = low_profile_screw_points) {
            translate(i) color("red") circle(d=screw_bolt_d, $fn=screw_bolt_fn);
        } 
    }
    for (i = [0:len(column_offsets)-1]) {
        a = i > 3 ? 1 : 0;
        translate([i*key_width, column_offsets[i]]) 
            for (i=[a:4]) {
                translate([0, i*key_width, 0])
                    square([key_width+2*border, key_width+2*border]);
            }
    }
    // additional space at the controller place
    translate([
        border + 4*key_width,
        //140
        2*border + 5*key_width + column_offsets[6]
    ])
        square([
            3*key_width + border,
            column_offsets[3] - column_offsets[4]]);
    // additional space beneath 4th column
    translate([2*border + 4*key_width - screw_bolt_d/2, column_offsets[2]])
        translate([-key_width, 0])
            square([key_width, screw_bolt_d]);
    // additional space for the left-most thumb screw
    translate([
            2*border + 4*key_width - screw_bolt_d/2,
            key_width - screw_bolt_d/2 + column_offsets[4]
        ])
        square(screw_bolt_d);
}


module finger_switches(thumb_col=false) {
    

    translate([border, border, 0])
        for (i = [0:len(column_offsets)-1]) {
            a = i > 3 ? 1 : 0;
            translate([i*key_width, column_offsets[i],0]) 
                for (i=[a:4]) {
                    translate(switch_offset)
                        translate([0, i*key_width, 0])
                            switch_hole();
                }
        }
}


module upper_plug_plate_slice() {
    height = c_plate_distance + 1;
    difference() {
        translate([
            controller_front_center_xy.x,
            controller_front_center_xy.y - c_plate_fixator_thickness/2 - wall,
            -height/2 - plate_thickness
        ]) {
            cube([
                c_plate_width + 2*c_plate_fixator_extra_width,
                c_plate_fixator_thickness,
                height
            ], center=true);
        }
        controller_part();
        translate([0, 0.1, 0]) controller_part();
    }
}


module case_connection_die() {
    width = c_plate_width + 2*c_plate_fixator_extra_width + 2;
    color("red") translate([
        controller_front_center_xy.x,
        controller_front_center_xy.y - wall - case_connection_die_length/2,
        -(plate_thickness + inner_height + bottom_thickness)
    ]) {
       cube([width, case_connection_die_length, 10], center=true);
    }

}

/*
 * One of the poles where the screws go through.
 */
module support_screw_bolt(height) {
    length = (key_width - switch_width) * 2;
    difference() {
        color("PowderBlue")
            cube([length, length, height]);
        color("yellow")
            translate([length/2, length/2, -0.01])
                cylinder(d=2, h=support_screw_hole_depth, $fn=15);
    }
}

/*
 * One of these little triangles at the side of the support screw poles
 */
module support_screw_stake(height, rotation) {
    width = switch_width / 4;
    color("PowderBlue") translate([
        key_width-switch_width,
        support_screw_bolt_stake/2 - (key_width-switch_width-support_screw_bolt_stake)/2,
        height
    ]) 
    rotate(rotation)
        translate([-(key_width-switch_width), support_screw_bolt_stake/2, 0])
            rotate(180, [0,1,0]) rotate(90, [1,0,0])
                linear_extrude(support_screw_bolt_stake) 
                    polygon([
                        [0,0],
                        [0,height],
                        [width,0]
                    ]);
}

/*
 * The main finger plate.
 */ 
module finger_plate() {
    difference() {
        translate([0,0,-plate_thickness])
            linear_extrude(plate_thickness) finger_plate_outline();
        translate([0,0,-switch_indent])
            finger_switches();
        translate([0,0,-plate_thickness-1]) color("red")
            linear_extrude(wall_nudge_height+1) {
                difference() {
                    offset(delta = wall_nudge_width)
                        finger_plate_outline();  
                    offset(delta = -wall_nudge_width - wall_nudge_w_clearance)
                        finger_plate_outline();
                }
            }
        for (i = low_profile_screw_points) {
            translate([i[0], i[1], 0])
                screw_flat_head(d=screw_d, dh=screw_head_d, ah=screw_head_a, l=plate_thickness+0.1, head_elongation=1);
        } 
    }
    upper_plug_plate_slice();

    for (i = support_screw_points) {
        height = i[5];
        translate([
            i[0][0] + border - (key_width-switch_width),
            i[0][1] + border - (key_width-switch_width),
            -plate_thickness - height
        ]) {
            support_screw_bolt(height);
            if (i[1]) {
                support_screw_stake(height, 270);
            }
            if (i[2]) {
                support_screw_stake(height, 180);
            }
            if (i[3]) {
                support_screw_stake(height, 90);
            }
            if (i[4]) {
                support_screw_stake(height, 0);
            }
        }
    }
}

/*
 * Tiny hole with cone indentation on the case bottom for the little support screws.
 */
module support_screw_hole(diameter, length_screw, length_head=10, head_angle=45) {
    upper_head_radius = length_head * tan(head_angle);
    head_indent = (diameter/2) * length_head / upper_head_radius;

    cylinder(h=length_screw, d=diameter, $fn=30);
    translate([0,0,length_screw - head_indent])
        cylinder(h=length_head, r1=0, r2=upper_head_radius, $fn=80);
}


module led_diffusor_stands() {
    translate([25,33,0])
    rotate(12, [0,0,1])
    for (i=[0:5]) {
        translate([i*16.666, 0, bottom_thickness]) {
            cylinder(d=2, h=4, $fn=12);
            translate([0, -12.4,0]) cylinder(d=2, h=4, $fn=12);
        }
    }
}


module _led_diffusor_base(){
    bridge_w = 0.6;
    channel_w = 0.6;
    pattern_w = bridge_w + channel_w;
    channel_h = 0.6;
    depth = 120;
    length = 140;
    x_bridge_distance = 14;
    xn = round(length / pattern_w);
    yn = round(depth / x_bridge_distance);
    color("green") cube([xn*pattern_w, depth, 0.2]);
    for(i=[0:xn-1]) {
        translate([i*pattern_w, 0, 0.2])
            cube([bridge_w, depth, channel_h]);
    }
    color("green") translate([0, 0, 0.2+channel_h]) cube([xn*pattern_w, depth, 0.2]);
    for(i=[0:xn-1]) {
        translate([i*pattern_w + pattern_w/2, 0, 2*0.2 + channel_h])
            cube([bridge_w, depth, channel_h]);
    }
    for(i=[0:yn]) {
        translate([0,i*x_bridge_distance, 2*0.2 + channel_h])
            cube([length, bridge_w, channel_h]);
    }
    color("green") translate([0, 0, 2*(0.2+channel_h)]) cube([xn*pattern_w, depth, 0.2]);
}

module led_diffusor() {
    translate([0,0,case_bottom_z() + bottom_thickness + 2])
    difference() {
        intersection() {
            _led_diffusor_base();
            translate([0,0,-5]) linear_extrude(10) offset(delta=-screw_bolt_d) finger_plate_outline(false);
        }
        finger_plate();
        translate([-2,0,15]) case_connection_die();
        translate([7,0,15]) case_connection_die();

        /*
        for (j = [0:3]) {
            i = support_screw_points[j];
            color("yellow")
                translate([
                    i[0][0] + border, 
                    i[0][1] + border 
                ]) {
                    translate([0,0,-5]) cylinder(d=9, h=10, $fn=30);
                }
        }
        translate([0,0,-bottom_thickness-0.1]) color("pink") led_diffusor_stands();
        */
    }

}

/*
 * The main case.
 */
module outer_case() {
    height = inner_height + bottom_thickness + wall_nudge_height;
    difference() {
        translate([0,0, -height - plate_thickness + wall_nudge_height]) {
            difference() {
                union() {

                    // case main body
                    difference() {
                        color("SteelBlue") linear_extrude(height)
                            finger_plate_outline();
                        color("IndianRed") 
                            translate([0,0,bottom_thickness])
                                linear_extrude(height)
                                    offset(delta = -wall) finger_plate_outline();
                    }

                    // screw bolt cylinders
                    for (i = low_profile_screw_points) {
                        translate([i[0],i[1],0])
                            color("SeaGreen")
                                cylinder(
                                    d=screw_bolt_d,
                                    h=inner_height + bottom_thickness,
                                    $fn=50
                                );
                    } 
                    
                    if (use_led_diffusor) {
                        led_diffusor_stands();
                    }
                    // LED stripe pillars
                    /*
                    d = 1000 / 60;
                    for (i =[0,1,2,4,5,6,7]) {
                        translate([
                            9 + i*d,
                            2*key_width + 3,
                            bottom_thickness
                        ])
                            stripe_pillar(49); 
                    }
                    */
                }

                // support screw holes
                for (j = [0:3]) {
                    i = support_screw_points[j];
                    translate([
                        i[0][0] + border, //- (key_width-switch_width),
                        i[0][1] + border, //- (key_width-switch_width),
                        5+1.4
                    ]) {
                        color("OrangeRed")
                            rotate(180,[1,0,0])
                                support_screw_hole(diameter=2.1, length_screw=5, length_head=5);
                    }
                }

                // nudge for finger plate
                color("OrangeRed") // nudge
                    translate([0, 0, height - wall_nudge_height - wall_nudge_h_clearance])
                        linear_extrude(height)
                            offset(delta = -wall_nudge_width) finger_plate_outline();
                
                // screw holes 
                color("Fuchsia") for (i = low_profile_screw_points) {
                    translate(i) {
                        translate([0,0,-1])
                            cylinder(d=screw_d, h=60, $fn=40);
                    }
                }
                // nut pits
                color("Fuchsia") for (j = screws_w_case_nut) {
                    i = low_profile_screw_points[j];
                    translate(i) {
                        translate([0,0,-1]) 
                            nut(nut_size, nut_height + 1);
                    }
                }
            }
        }

        // connection slots for the thumb cluster
        color("red") {
            thumb_connectors(cutaway=true);
            // and some little clearance
            translate([0.1, 0.1, 0]) thumb_connectors(cutaway=true);
            translate([-0.1, -0.1, 0]) thumb_connectors(cutaway=true);
        }

        // big cut away on the bottom for the controller
        case_connection_die();

        // small front cutaway for the left-most thumb conncetion cylinder to make it accesible from the front
        translate([-screw_bolt_d/2, -screw_bolt_d, tc_middle_cylinder_offset - tc_tray_clearance/2])
            translate(low_profile_screw_points[10])
                color("pink")cube([screw_bolt_d, screw_bolt_d, tc_middle_cylinder_height + tc_tray_clearance]);
    }

}

/*
 * The support screws for the controller connector part. Used as cut-away body to make the holes.
 */
module controller_support_screws() {
    for (j = [4:5]) {
        i = support_screw_points[j];
        translate([
            i[0][0] + border, //- (key_width-switch_width),
            i[0][1] + border, //- (key_width-switch_width),
            -plate_thickness - c_plate_thickness - c_plate_distance - c_mount_plate_thickness + 5 + 1.5 
        ]) {
            color("OrangeRed") {
                rotate(180,[1,0,0]) {
                    support_screw_hole(diameter=2.3, length_screw=5.6, length_head=2.1);
                    translate([0, 0, 6.45])
                        cylinder(r=2.5, h=15, $fn=30);
                }
            }
        }
    }
}

/*
 * The thick plate of the controller part where the USB slot gets punched in. (But not yet in this module.)
 */
module _raw_plug_plate() {
    translate(controller_part_anchor)
        rotate(90, [0,0,1]) rotate(180, [0,1,0]) 
            translate([-plug_plate_thickness, -c_plate_width/2, -c_plate_thickness])
                cube([plug_plate_thickness, c_plate_width, plug_plate_height], center=false);
}

/*
 * The controller part.
 */
module controller_part() {
    difference() {
        union() {
            _raw_plug_plate();
            translate(controller_part_anchor) {
                rotate(90, [0,0,1]) rotate(180, [0,1,0]) {
                    color("DarkTurquoise") {
                        promic_mount(false);
                        translate([c_plate_length/2, 0, -c_plate_thickness/2])
                            cube([c_plate_length, c_plate_width, c_plate_thickness], center=true);
                    }
                }
            }
        }
        controller_support_screws();
        translate(controller_part_anchor)
            rotate(90, [0,0,1]) rotate(180, [0,1,0])
                promic_front_cutaway();
    }
}


function controller_mount_screw_length() = -1*(case_bottom_z() - c_tray_inner_height - controller_part_anchor.z) - c_mount_screw_elevation + bottom_thickness; 

/*
 * The controler mount part without the support screw holes and in center position.
 */
module _raw_controller_mount_part() {
    difference() {
        length = c_plate_length - promic_length + promic_front_notch - 0.2;
        height = -1*(case_bottom_z() - c_tray_inner_height - controller_part_anchor.z); // total height, used for the screw cylinder which goes throuout the part
        color("SeaGreen") union() {
            translate([0, -length + screw_bolt_d/2, 0]) {
                translate([0, length/2, c_mount_plate_thickness/2])
                    cube([c_plate_width, length, c_mount_plate_thickness], center=true);
                translate([0, 4/2, 5/2])
                    cube([c_plate_width, 4, 5], center=true);
                translate([0, length - (length-2)/2 , 6/2])
                    cube([13, length-2, 6], center=true);
            }
            cylinder(d=screw_bolt_d, h=height, $fn=30);
        }
        color("Fuchsia") translate([0, 0, c_mount_screw_elevation - c_mount_nut_z_clearance]) {
            translate([-nut_size/2, 0, 0])
                cube([nut_size + 0.2, 12, nut_height + c_mount_nut_z_clearance]);
            rotate(30)
                nut(nut_size, nut_height+0.4);
            translate([0,0,-8])
                cylinder(h=30, d=screw_d, $fn=30);
        }
    }
}

/*
 * The controller mount part.
 */
module controller_mount_part() {
    difference() {    
        translate(controller_mount_screw_xy) {
            translate([0, 0, controller_part_anchor.z]) {
                rotate(180,[1,0,0]) {
                    _raw_controller_mount_part();
                }
            }
        }
        color("DarkOrange") controller_support_screws();
        translate([0,-0.2,0]) translate(controller_part_anchor) {
            rotate(90, [0,0,1]) rotate(180, [0,1,0]) {
                promic_back_notch();
            }
        }
    }
}

/*
 * 2D polygon of the controller tray's outer shape (without stand extension).
 */
module _raw_controller_tray__outer_polygon() {
    lw=c_tray_lower_width;
    uw=c_tray_upper_width;
    bh=c_tray_base_height;
    uh=c_tray_upper_height;
    polygon([
        [-lw/2, 0],
        [-lw/2, bh],
        [-uw/2, uh+bh],
        [uw/2, uh+bh],
        [lw/2, bh],
        [lw/2, 0]
    ]);
}

/*
 * The basic tray body, solid.
 */
module _outer_controller_tray_body() {
    linear_extrude(c_tray_length)
        _raw_controller_tray__outer_polygon();
}

split_jack_mount_width = 11.2;
split_jack_mount_inner_length = 10;
jack_distance = case_connection_die_length + 10;
jack_mount_outer_bolt_d = 11.2;
jack_mount_outer_bolt_l = 13;

/*
 * The cut-away for the jack.
 */
module _jack_socket() {
    translate([
        c_tray_lower_width/2-split_jack_mount_inner_length,
        jack_mount_outer_bolt_d/2,
        c_tray_length - jack_distance
    ])
        rotate(90,[0,1,0]) {
            // jack chassis socket die
            translate([0, 0, -16 + jack_mount_outer_bolt_l + 2.5]) {
                color("Khaki")
                   cylinder(d=jack_diameter, h=11.8, $fn=40);
                color("Tomato")
                    cylinder(d=jack_front_diameter, h=16, $fn=40);
            }
        }
}

/*
 * The extra block inside the tray where the jack is mounted in. (Without the jack cut-away.)
 */
module _split_jack_mount() {
    intersection() {
        _outer_controller_tray_body();
        translate([
            c_tray_lower_width/2 - split_jack_mount_inner_length,
            0,
            c_tray_length - split_jack_mount_width/2 - jack_distance
        ]) 
            cube([split_jack_mount_inner_length, c_tray_inner_height, split_jack_mount_width]);
    }
    translate([
        c_tray_lower_width/2-split_jack_mount_inner_length,
        jack_mount_outer_bolt_d/2,
        c_tray_length - jack_distance
    ])
        rotate(90,[0,1,0]) {
            cylinder(d=jack_mount_outer_bolt_d, h=jack_mount_outer_bolt_l, $fn=50);
            translate([-jack_mount_outer_bolt_d/2, -jack_mount_outer_bolt_d/2,0])
                cube([jack_mount_outer_bolt_d, jack_mount_outer_bolt_d/2, jack_mount_outer_bolt_l]);
        }
}

/*
 * The big cut-away for the hole that connects the main case and the tray.
 */
module controller_tray_passage_die() {
    translate([controller_front_center_xy.x, controller_front_center_xy.y, case_bottom_z()]) {
        translate([0, -c_tray_length, 0]) rotate(270,[1,0,0]) {
            color("DarkViolet") {
                translate([
                    -16 + 1*wall,
                    wall - 1,
                    -20 
                ])
                    cube([
                        2*key_width - border - 1.5*screw_bolt_d - 2*wall,
                        0.7*(c_tray_base_height + c_tray_upper_height) - 2*wall + 2,
                        29
                    ]);
            }
        }
    }
}

/*
 * The cut-away for the hole which connects the tray and the thumb cube.
 */
module _controller_tray_thumb_passage() {
    connector_length =  4*key_width + 2*border + column_offsets[3] - column_offsets[4]  - c_tray_length; 
    difference() {
        union() {
            // taper body
            color("SkyBlue") {
                translate([-12,0,0]) {
                    mirror([0,0,1]) {
                        linear_extrude(connector_length, scale=[0.4, 0.7])
                            translate([12,0,0])
                                _raw_controller_tray__outer_polygon();
                    }
                }
            }
            // cube body
            color("Turquoise") {
                translate([
                    -16,
                    0,
                    -connector_length
                ])
                    cube([
                        2*key_width - border - 1.5*screw_bolt_d,
                        0.7*(c_tray_base_height + c_tray_upper_height),
                        connector_length
                    ]);
            }
        }
        // taper cut-away
        color("MediumVioletRed") {
            translate([-12,0, 0.1]) {
                mirror([0,0,1]) {
                    linear_extrude(connector_length + 0.2, scale=[0.4, 0.7])
                        translate([12,0,0])
                            offset(delta=-1*wall) _raw_controller_tray__outer_polygon();
                }
            }
        }
    }
}

/*
 * The basic controller tray, without the brim, the screw and reset hole.
 */
module _raw_controller_tray() {
    echo(c_tray_inner_height=c_tray_inner_height);
    translate([0, -c_tray_length, 0]) rotate(270,[1,0,0]) {
        difference() {
            union() {
                difference() {
                    // outer case
                    color("SandyBrown")
                        _outer_controller_tray_body();
                    // inner cut-away
                    color("DeepPink") {
                        translate([0, 0, -wall - plug_plate_thickness])
                            linear_extrude(c_tray_length) {
                                offset(delta = -bottom_thickness)
                                    _raw_controller_tray__outer_polygon();

                            }
                        translate([0, c_tray_base_height/2 - 0.4, c_tray_length/2 - wall - plug_plate_thickness])
                            cube([c_tray_lower_width - 2*bottom_thickness, c_tray_base_height, c_tray_length], center=true);
                    }
                }
                _split_jack_mount();
            }
            _jack_socket();
        }
        _controller_tray_thumb_passage();
    }


}

/*
 * The quarter-circle shape that stabilizes the screw fingers on the outer side of the tray.
 */
module _c_tray_brim_half_circle() {
    r = screw_bolt_d + 3.5;
    translate([-r,0,0]) intersection() {
        difference() {
            cube([2*r,2*r,c_tray_brim_con_height_wo_nut]);
            translate([r, r, -5]) cylinder(r=r, h=10, $fn=50);
        }
        cube([r, r, 5]);
    }
}

/*
 * The “brim” around the tray with screw fingers and stuff. (But without screw/nut holes.)
 */
module controller_tray_brim() {
    difference() {

        union() {
            translate([0,0,case_bottom_z() - c_tray_brim_con_height_wo_nut]) {
                translate(low_profile_screw_points[14])
                    translate([0, +screw_bolt_d/2, 0])
                        _c_tray_brim_half_circle();
                translate(low_profile_screw_points[14])
                    translate([0, -screw_bolt_d/2, 0])
                        mirror([0,1,0])
                            _c_tray_brim_half_circle();
                translate(low_profile_screw_points[13])
                    translate([0, +screw_bolt_d/2, 0])
                        _c_tray_brim_half_circle();
            }

            // cylinders around screws
            for (i = screws_w_c_tray_connection) {
                height = i[1] ? c_tray_brim_con_height_w_nut : c_tray_brim_con_height_wo_nut;
                translate(low_profile_screw_points[i[0]]) {
                    translate([0,0,case_bottom_z() - height]) {
                        cylinder(d=screw_bolt_d, h=height, $fn=screw_bolt_fn);
                    }
                }
            }
            // cubes for the front screws
            translate(low_profile_screw_points[5])
                translate([
                    screw_bolt_d/2,
                    0,
                    case_bottom_z() - c_tray_brim_con_height_w_nut/2
                    ])
                    cube([screw_bolt_d, screw_bolt_d, c_tray_brim_con_height_w_nut], center=true);
            translate(low_profile_screw_points[6])
                translate([
                    -screw_bolt_d/2,
                    0,
                    case_bottom_z() - c_tray_brim_con_height_w_nut/2
                    ])
                    cube([screw_bolt_d, screw_bolt_d, c_tray_brim_con_height_w_nut], center=true);
            // connector for the middle right screw
            translate(low_profile_screw_points[14]) {
                l = screw_bolt_d + 5;
                translate([
                    -l/2,
                    0,
                    case_bottom_z() - c_tray_brim_con_height_w_nut/2
                    ])
                    cube([l, screw_bolt_d, c_tray_brim_con_height_w_nut], center=true);
            }
            // connector for right thumb screw (no 13)
            translate(low_profile_screw_points[13]) {
                translate([0, -screw_bolt_d/2, case_bottom_z() - c_tray_brim_con_height_w_nut]) {
                    difference() {
                        translate([-25,0,0])
                            cube([25, screw_bolt_d, c_tray_brim_con_height_w_nut]);
                        translate([-screw_bolt_d/2 - 0.2, -screw_bolt_d/2, -c_tray_brim_con_height_wo_nut])
                            cube([screw_bolt_d, screw_bolt_d, c_tray_brim_con_height_w_nut]);
                    }
                }
                translate([-21.5, screw_bolt_d/2, case_bottom_z() - c_tray_brim_con_height_w_nut])
                    cube([10,10,c_tray_brim_con_height_w_nut]);
            }
            // connector for left thumb screw (no 11)
            translate(low_profile_screw_points[11]) {
                translate([-screw_bolt_d/2, 0, case_bottom_z() - c_tray_brim_con_height_w_nut]) {
                    translate([screw_bolt_d/2+2,30,0]) {
                        difference() {
                            cylinder(h=c_tray_brim_con_height_w_nut, d=screw_bolt_d+4, $fn=50);
                            translate([0,-10, -1])
                                color("red") cube([20, 20, c_tray_brim_con_height_w_nut + 2]);
                        }
                    }
                    cube([screw_bolt_d, 30, c_tray_brim_con_height_w_nut]);
                }
            }
        } //union

        case_connection_die();

        // cylinders cut-aways above nut-less cylinders (screws 11 and 13)
        for (i = screws_w_c_tray_connection) {
            if (! i[1]) {
                translate(low_profile_screw_points[i[0]]) {
                    translate([0,0,case_bottom_z() - 10 - c_tray_brim_con_height_wo_nut]) {
                        cylinder(d=screw_bolt_d+0.4, h=10, $fn=screw_bolt_fn);
                    }
                }
            }
        }
        // cylinder cut-away above middle thumb screw
        translate(low_profile_screw_points[12]) {
            translate([0,0,case_bottom_z() - 9.9]) {
                cylinder(d=screw_bolt_d+0.4, h=10, $fn=screw_bolt_fn);
            }
        }
    }
}

/*
 * Screw and nut's of the tray, used as cut-away.
 */
module controller_brim_screw_cut_away() {
    // cut away screw and nut holes
    for (i = screws_w_c_tray_connection) {
        height = i[1] ? c_tray_brim_con_height_w_nut : c_tray_brim_con_height_wo_nut;
        translate(low_profile_screw_points[i[0]]) {
            translate([0,0,case_bottom_z() - height]) {
                    translate([0,0,-0.5])
                        cylinder(d=screw_d, h=20, $fn=20);
                    if (i[1]) {
                        translate([0,0,-1])
                            nut(nut_size, nut_height + 1);
                    }
            }
        }
    }
}

/*
 * The controller tray part.
 */
module controller_tray() {
    difference() {
        left_right_clearance = 0.1;
        upper_clearance = 0.2;
        front_clearance = 0.2;
        union() {
            translate([controller_front_center_xy.x, controller_front_center_xy.y, case_bottom_z()]) {
                _raw_controller_tray();
                translate([-c_tray_upper_width/2, -c_tray_length, -50 - c_tray_inner_height])
                    cube([c_tray_upper_width, c_tray_length, 50]); 
            }
            controller_tray_brim();
        }
        translate([left_right_clearance, 0, 0]) _raw_plug_plate();
        translate([-left_right_clearance, 0, 0]) _raw_plug_plate();
        translate([0, front_clearance, 0]) _raw_plug_plate();
        translate([0, 0, -upper_clearance]) _raw_plug_plate();
        translate([0, -0.1, 0]) _raw_plug_plate();
        translate([controller_front_center_xy.x, controller_front_center_xy.y, case_bottom_z()])
            translate([0,0.1,0]) rotate(90,[1,0,0]) linear_extrude(wall+0.2)
                circle(10,$fn=10);
        translate(controller_mount_screw_xy)
            translate([0,0, case_bottom_z() - c_tray_inner_height - bottom_thickness])
                rotate(180,[1,0,0]) screw_flat_head(d=screw_d, dh=screw_head_d, ah=90, l=10, head_elongation=60);
        // reset hole

        translate(controller_part_anchor) translate([0,-25,-40])
            cylinder(h=30, d=3, $fn=20);
        
        // stand level
        stand_cutaway();

        // thumb passage cube cut-away
        controller_tray_passage_die();

        // screws and nuts
        controller_brim_screw_cut_away();
    }
}



module v_help_line_controller_anchor() {
    translate([controller_part_anchor.x, controller_part_anchor.y, -100]) {
        color("red")
            cylinder(d=1, h=130);
        rotate (90,[1,0,0]) linear_extrude(0.1) text("controller anchor", size=2);
    }
}


module v_help_line_controller_mount_screw() {
    translate([controller_mount_screw_xy.x, controller_mount_screw_xy.y, -100]) {
        color("red")
            cylinder(d=1, h=130);
        rotate (90,[1,0,0]) linear_extrude(0.1) text("controller mount screw", size=2);
    }
}



module v_help_line_controller_front_center() {
    translate([controller_front_center_xy.x, controller_front_center_xy.y, -130]) {
        color("red")
            cylinder(d=1, h=150);
        rotate (90,[1,0,0]) linear_extrude(0.1) text("controller front center", size=2);
    }
}
 

module _h_help_line(v, length, label, positive=true, lcolor="red") {
    rotation = positive ? -90 : 90;
    translate(v)
        rotate(rotation, [1,0,0]) {
            color(lcolor) cylinder(d=0.5, h=length);
            translate([0,0,length]) linear_extrude(0.1) text(label, size=2);
        }
}


module h_help_line_controller_anchor() {
    translate(controller_part_anchor)
        translate([0, 120 - c_plate_length/2, 0])
            rotate(270, [1,0,0]) {
                color("red")
                    translate([0,0,-120]) cylinder(d=1, h=120);
                linear_extrude(0.1) text("controller anchor z", size=2);
            }
}


module h_help_line_case_bottom() {
    translate([45, 110, case_bottom_z()])
        translate([0, 120 - 30, 0])
            rotate(270, [1,0,0]) {
                color("red")
                    translate([0,0,-120]) cylinder(d=1, h=120);
                linear_extrude(0.1) text("case bottom z", size=2);
            }
}

module h_help_line_bottom_minus_tray_height() {
    _h_help_line(
        [
            controller_part_anchor.x,
            controller_part_anchor.y,
            case_bottom_z() - c_tray_inner_height 
        ],
        length=100,
        label="bottom - c_tray_inner_height",
        lcolor="Orange"
    );
}



module v_labels() {
    v_help_line_controller_anchor();
    v_help_line_controller_mount_screw();
    v_help_line_controller_front_center();
}


module h_labels() {
    h_help_line_controller_anchor();
    h_help_line_case_bottom();
    h_help_line_bottom_minus_tray_height();
}


//translate([0,0,-20])
    controller_part();
finger_plate();
//thumb_plate();

//translate([0,0,-50])
    outer_case();
//controller_support_screws();
controller_mount_part();



//finger_case_outline();
//finger_switches();
//thumb_switches();
//case_connection_die();
