include <../parameters.scad>
use <lib/nut.scad>

screw_height = inner_height + tc_lower_cylinder_height;

function thumb_connector_screw_length() = -case_bottom_z() + tc_lower_cylinder_height;

module thumb_connectors(cutaway=false) {
    lower_cylinder_offset = -tc_lower_cylinder_height - plate_thickness - inner_height - bottom_thickness;
    for (thumb_screw = thumb_screw_connections) {
        position =low_profile_screw_points[thumb_screw[0]];
        direction = thumb_screw[1];
        clearance = cutaway ? tc_middle_cylinder_clearance : 0;
        translate([position.x, position.y, 0]) {
            difference() {
                union() {
                        lch = thumb_screw[0] == 11 || thumb_screw[0] == 13
                              ? tc_lower_cylinder_height - c_tray_brim_con_height_wo_nut - tc_tray_clearance
                              : tc_lower_cylinder_height;
                    // middle cylinder with connector
                    translate([0,0,tc_middle_cylinder_offset - clearance]) {
                        cylinder(d=screw_bolt_d, h=tc_middle_cylinder_height + 2*clearance, $fn=screw_bolt_fn);
                        rotate(-90*direction, [0,0,1]) {
                            translate([0, - screw_bolt_d/2, 0])
                                cube([screw_bolt_d/2, screw_bolt_d, tc_middle_cylinder_height + 2*clearance]);
                        }
                    }
                    // lower cylinder with connector
                    translate([0,0,lower_cylinder_offset]) {
                        cylinder(d=screw_bolt_d, h=lch, $fn=screw_bolt_fn);
                        rotate(-90*direction, [0,0,1]) {
                            translate([0, - screw_bolt_d/2, 0])
                                cube([screw_bolt_d/2, screw_bolt_d, lch]);
                        }
                    }
                } // union of cylinders
                // nut holes
                color("red")
                    translate([0, 0, lower_cylinder_offset - 1])
                        nut(nut_size, nut_height + 1);
                            
                // screw holes
                if (!cutaway) {

                    color("red")
                        translate([0,0, tc_middle_cylinder_offset - screw_height + tc_middle_cylinder_height + 0.1])
                            cylinder(d=screw_d, h=screw_height, $fn=20);
                }

            } // difference with screw hole
        }
    }
}

thumb_connectors();
