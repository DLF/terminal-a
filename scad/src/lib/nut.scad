module hexagon(d) {
	ra = d/2 * 2/sqrt(3);
	circle(r = ra, $fn=6);
}

module nut(size, height) {
    linear_extrude(height)
        hexagon(size);
}

