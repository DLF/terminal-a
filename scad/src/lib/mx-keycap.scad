
module mx_stamp() {
    // connector stamp
    translate([0,0,0]) difference(){
        union(){
            cylinder (4,2.75,2.75, $fn=30);
            translate([0,0,2]) cube([6,3.2,4], center=true);
        }
        color("red") translate([0,0,2]) {
            cube([1.3,4.2,4.1],center=true);
            cube([4.2,1.4,4.1],center=true);
        }
    }
}

module mx_cut_away() {
    color("yellow") translate([0,0,-0.1])
        linear_extrude(height=3.8, scale=0.8)
            square([14,14], center=true);
}

module _top_slant(width, length, height, radius=0, squeeze=0.5, indent=1, tilt=0) {
    my_height = length;
    radius = radius == 0 ? width-1 : radius;
    tilt_offset = tan(tilt) * length/2;     // the offset needed for the slant die at x/y = 0/0
    translate([0,0,height+tilt_offset]) {
        rotate(tilt, [1,0,0]) {
            color("LightSalmon") translate([0, 0, my_height/2])
                cube([2*width, 2*length, my_height], center=true);
            color("SandyBrown")
                translate([0, length, squeeze*radius - indent]) rotate(90, [1,0,0])
                    scale([1, squeeze, 1]) cylinder(r=radius, h=2*length, $fn=100);
        }
     }
}

module mx_cap_std() {
    // Keycap shell
    difference() {
        difference() {
            // outer body
            linear_extrude(height=5, scale=0.8)
                translate([-6.5,-6.5,0]) minkowski()
                {
                    square([13,13]);
                    circle(2, $fn=30);
                }
            // round finger cut-away
    //		translate([0,0,9.2]) scale([1,1,0.5]) color("green") sphere(10, $fn=100);
        }
        // cut-away within the keycap
        mx_cut_away();
    }
    mx_stamp();
}

module mx_cap_square(
    width=17.5,
    length=17.5,
    height=5,
    shrink_factor=0.8,
    tilt=0,
    corner_r=2.5,
    wall=1,
    indent=0.8,
    squeeze=0.5,
    x_offset=0,
    y_offset=0,
    indent_x_offset=0
) {
    //rounded square dimensions
    rs_w = width - 2*corner_r;
    rs_l = length - 2*corner_r;
    tilt_height = tan(tilt) * length;
    difference() {
        union() {
            //shell
            difference() {
                // outer body
                linear_extrude(height=height+tilt_height, scale=shrink_factor) {
                    minkowski()
                    {
                        square([rs_w, rs_l], center=true);
                        circle(corner_r, $fn=30);
                    }
                }
                // inner cut-away
                color("Pink") translate([0,0,-0.1]) linear_extrude(height=height-wall-indent, scale=shrink_factor) {
                    minkowski()
                    {
                        square([rs_w-2*wall, rs_l-2*wall], center=true);
                        circle(corner_r, $fn=30);
                    }
                }
            }
            // stamp support
            difference() {
                linear_extrude(height=height, scale=shrink_factor) {
                    {
                        square([width, 7], center=true);
                        square([7, length], center=true);
                    }
                }
                cube([width + 1, length + 1, 6], center=true);
            }
        }
        // inner minimal cut-away for the switch (yellow)
        translate([x_offset, y_offset])
            mx_cut_away();
        // top finger cut-away (potentially slanted)
        translate([indent_x_offset, 0])
            _top_slant(width=width*shrink_factor, length=length, height=height, indent=indent, squeeze=squeeze, tilt=tilt);
    }
    translate([x_offset, y_offset])
        mx_stamp();
}

//mx_cap_std();
difference() {
    //mx_cap_square(length=28, height=5);
    mx_cap_square(width=19.5, height=7, tilt=10, x_offset=1, indent_x_offset=1);
//    translate([0,-25,0]) cube([50,50,50], center=true);
}

//translate([18,0,0]) mx_cap_std();
