stripe_width = 10;
width = 3;
slot_depth = 0.6;
back_plate_depth = 2;
back_plate_support_depth = 1.2;
back_plate_support_width = 2;
back_plate_support_height_factor = 0.5;


module stripe_pillar(angle=45) {
    height = sin(angle) * stripe_width;
    depth = cos(angle) * stripe_width;
    slot_offset = slot_depth / sin(angle);
    back_plate_offset = back_plate_depth / sin(angle);
    difference() {
        union() {
            // main body with tilted back
            difference() {
                translate([0, -slot_offset - back_plate_offset, 0])
                    cube([width, depth + slot_offset + back_plate_offset, height]);

                translate([0, -slot_offset - back_plate_offset, 0])
                    rotate(-(90-angle),[1,0,0]) 
                        translate([-1, -3*back_plate_depth, -0.3*height])
                            color("coral") cube([width+2, 3*back_plate_depth, 2*height]);
            }

            s_d = back_plate_support_depth + slot_offset + back_plate_offset;
            translate([(width - back_plate_support_width)/2, -s_d, 0])
            cube([back_plate_support_width, s_d, height * back_plate_support_height_factor]);
        }
        //slot
        rotate(-(90-angle),[1,0,0]) 
            translate([-1, -slot_depth, -0.3*height])
                color("pink") cube([width+2, slot_depth, 2*height]);
    }
}

stripe_pillar(50);
