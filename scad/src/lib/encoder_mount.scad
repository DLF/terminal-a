// Overall plate area needed: 15×15mm

module encoder_mount() {
    translate([7.5, 7.5, -0.75]) {
        difference(){
            color("blue") cube([13.5, 14.5, 1.5], center=true);
            color("yellow") cube([12.5, 13.5, 5.2], center=true);
//            color("red") translate([0,0,-2.1]) cube([19.2,7,3.2], center=true);
        }
    }
}

module encoder_hole() {
    color("red") translate([7.5,7.5,-1]) cylinder(10, 7.3/2, 7.3/2, $fn=30);
}




difference() {
    union() {
        encoder_mount();
        translate([7.5, 7.5, 1]) cube([20,20,2], center=true);
    }
    translate([0,0,0]) encoder_hole();
}


