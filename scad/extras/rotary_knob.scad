lower_h = 8;
upper_h = 9;
lower_d = 24;
upper_d = 32.5;
rounding_r = 2;


/*
 * “Quarter Inverted Circle”
 * A square with a quarter cirdle cut-away.
 */
module _qic(r) {
    difference() {
        square([r,r]);
        circle(r=r, $fn=80);
    }
}

module _rk_2d_base_form(
    lower_h,
    upper_h,
    lower_d,
    upper_d,
    rounding_r,
    cutaway_r
    )
{
    difference() {
        union() {
            square([lower_d / 2, lower_h]); 
            translate([0,+lower_h])
                square([upper_d / 2, upper_h]);

            delta = (upper_d - lower_d)/2 - rounding_r;
            translate([delta + lower_d/2, lower_h - delta])
            mirror([1,0])
                _qic(delta);
        }

        color("red")
            translate([upper_d/2 - rounding_r + 0.01, lower_h + upper_h - rounding_r + 0.01])
                _qic(rounding_r);

        color("red")
            translate([upper_d/2 - rounding_r + 0.01, lower_h + rounding_r - 0.01])
                rotate(-90)
                    _qic(rounding_r);

    }
}


module _ripple_cut_away(d, height, s=0.3, fn=20) {
    scale([1, s, 1])
    translate([0, -0.5*d, 0]) difference() {
        union(){
            cylinder(d=d, h=height, $fn=fn);
            translate([-2*d, 0, 0])
                cube([4*d, d, height]);
        }
        for (i=[1, -1]) {
            translate([-d*i, 0, -1])
                cylinder(d=d, h=height+2, $fn=fn);
            translate([-2*d*i, 0, 0.5*height])
                cube([2*d, d, height+2], center=true);
        }
    }
}


module solid_rotary_knob(
    lower_h,
    upper_h,
    lower_d,
    upper_d,
    rounding_r,
    cutaway_r,
    finger_pit_d = 8,
    finger_pit_depth = 1,
    finger_pit_offset = 0,
    finger_pit_angles = [0, 120, 240]
    )
{
    difference() {
        rotate_extrude($fn=48) {
            _rk_2d_base_form(
                lower_h,
                upper_h,
                lower_d,
                upper_d,
                rounding_r,
                cutaway_r
            );
        }
        // substract ripples
        ripple_angle = 15;
        angle_offset = 7.5;
        for(a = [1 : 360/ripple_angle])
            rotate(ripple_angle * a + angle_offset)
            translate([0, upper_d/2, lower_h - 2])
                _ripple_cut_away(2.5, upper_h + 4, fn=10);
        // substract finger pits
        finger_pit_offset = finger_pit_offset == 0 ? (upper_d - finger_pit_d)/2 - rounding_r - 0.4 : finger_pit_offset;
        color("orange")
            for (angle = finger_pit_angles)
                rotate(angle)
                    translate([finger_pit_offset, 0, lower_h + upper_h])
                        scale([1, 1, finger_pit_depth/(finger_pit_d/2)])
                            sphere(d=finger_pit_d, $fn=40);
    }
}

module generic_bolt_cut_away(
    base_d,
    base_h,
    bolt_d,
    bolt_h,
    bolt_indent,
    bolt_indent_offset,
    reduction_d,
    reduction_h,
    reduction_positions
) {
    cylinder(d=base_d, h=base_h, $fn=30);
    difference() {
        cylinder(d=bolt_d, h=bolt_h, $fn=30);
        translate([-bolt_d/2, bolt_d/2 - bolt_indent, bolt_indent_offset])
            cube([bolt_d, bolt_d, bolt_h + 2]);
        for(i=reduction_positions) {
            translate([0, 0, i]) difference() {
                cylinder(d=bolt_d+2, h=reduction_h, $fn=30);
                translate([0,0,-1])
                    cylinder(d=reduction_d, reduction_h + 2, $fn=30);
            }
        }
    }
}

difference() {
    solid_rotary_knob(
        lower_h = lower_h,
        upper_h = upper_h,
        lower_d = lower_d,
        upper_d = upper_d,
        rounding_r = rounding_r,
        cutaway_r = 5,
        finger_pit_angles = []
    );

    translate([0,0,-0.1]) generic_bolt_cut_away(
        base_d = 13,
        base_h = 3,
        bolt_d = 6 + 0.2, // 6 is the real d, 0.5 is clearance
        bolt_h = 16 - 1,
        bolt_indent = 1.5,
        bolt_indent_offset = 5.5,
        reduction_d = 6 + 0,
        reduction_h = 0.6,
        reduction_positions = [3, 5.5, 7, 9.5, 11, 12.5, 14]
    );
}
