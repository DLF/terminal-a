module led_lens(d=10, h=4.4, led_a=5.2, led_b=5.2, led_h=1.2, concave_f=0.4, side_cutaway_w=2.6, side_cutaway_h=1.2, extra_inner_sphere_d = 1, fn=50) {
    difference() {
        //main sphere
        intersection() {
            scale([1,1,h/(d/2)])
                sphere(d=d, $fn=fn);
            translate([0,0,h])
                cube([2*d, 2*d, 2*h], center=true);
        }
        // minus led cut-away
        color("red")
            translate([0,0,led_h/2 - 0.5])
                cube([led_a, led_b, led_h + 1], center=true);
        // minus inner sphere (lower concave surface)
        {
            i_h = (h - led_h)*concave_f;
            i_d = 2*sqrt(
                pow(led_a/2,2) + pow(led_b/2,2)
            ) + extra_inner_sphere_d;
            color("yellow") translate([0,0,led_h-0.01])
                intersection() {
                    scale([1,1,i_h/(i_d/2)])
                        sphere(d=i_d, $fn=fn);
                    translate([0,0,i_h])
                        cube([2*i_d, 2*i_d, 2*i_h], center=true);
                }
        }
        // minus outer sphere (upper concave surface)
        {
            o_h = (h - led_h)*concave_f;
            o_d = d;
            color("yellow") translate([0,0,h])
                intersection() {
                    scale([1,1,o_h/(o_d/2)])
                        sphere(d=o_d, $fn=fn);
                    translate([0,0,-o_h])
                        cube([2*o_d, 2*o_d, 2*o_h], center=true);
                }
       }
       // minus side cut-away
       translate([0, -side_cutaway_w/2, -0.001])
           cube([d, side_cutaway_w, side_cutaway_h]);
    }
}

module led_lens2(d=10, h=4.4, led_a=5.4, led_b=5.4, led_h=1.2, concave_f=0.4, fn=50) {
    difference() {
        //main cyl
        cylinder(d=d, h=h, $fn=fn);
        // minus led cut-away
        color("red")
            translate([0,0,led_h/2 - 0.5])
                cube([led_a, led_b, led_h + 1], center=true);
        // minus inner sphere (lower concave surface)
        {
            i_h = (h - led_h)*concave_f;
            i_d = d - 2; 
            color("yellow") translate([0,0,led_h-0.01])
                intersection() {
                    scale([1,1,i_h/(i_d/2)])
                        sphere(d=i_d, $fn=fn);
                    translate([0,0,i_h])
                        cube([2*i_d, 2*i_d, 2*i_h], center=true);
                }
        }
        // minus outer sphere (upper concave surface)
        {
            o_h = (h - led_h)*concave_f;
            o_d = d * 1.2;
            color("yellow") translate([0,0,h+0.01])
                intersection() {
                    scale([1,1,o_h/(o_d/2)])
                        sphere(d=o_d, $fn=fn);
                    translate([0,0,-o_h])
                        cube([2*o_d, 2*o_d, 2*o_h], center=true);
                }
       }
    }
}

module led_lens3(d=10, h=4.4, led_a=5, led_b=5.4, led_h=1.2, concave_d=10, concave_h=2.4, fn=50) {
    difference() {
        //main cyl
        cylinder(d=d, h=h, $fn=fn);
        // minus led cut-away
        color("red")
            translate([0,0,led_h/2 - 0.5])
                cube([led_a, led_b, led_h + 1], center=true);
        translate([0,0,h-concave_h+0.01]) color("yellow")
            cylinder(d1=0, d2=concave_d, h=concave_h, $fn=fn);

    }
}


intersection() {
//    translate([0,25,0]) cube([50,50,50], center=true);
    led_lens(concave_f=0.35);
}
