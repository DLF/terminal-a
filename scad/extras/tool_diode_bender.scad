depth = 14;
form_height = 3;
diode_d = 1.8;
diode_l = 3.6;
lead_d = 0.6;
loop_width = 1.2;
diode_offset = 4;
extra_indent = 0.4;
lead_connection_length = 18 + 4; // 18 is the switch width, 4 is the overlap with the next wire


v_1 = 10;

lead_indent = (diode_d - lead_d)/2;

ind = lead_indent + extra_indent;

counter_ind = form_height - ind;

diode_block_dx = 6;
lead_block_dy = 6;


module form_block() {
    y0 = depth - v_1 - 0.5;
    // diode block
    translate([0, y0, 0]) difference() {
        cube([diode_block_dx, v_1, form_height]);
        // - lead
        lead_ox = (diode_block_dx - lead_d)/2;
        color("red") translate([lead_ox, -1, form_height - ind]) cube([lead_d, v_1 + 2, ind + 1]);
        // - diagonal cut-away
        color("pink") translate([diode_block_dx - lead_ox , 0, counter_ind]) rotate(45, [0,0,1]) translate([0,-diode_block_dx,0]) cube([diode_block_dx, diode_block_dx, form_height + 1]);
        // - loop cut-away
        color("turquoise") translate([diode_block_dx - lead_ox + loop_width, v_1 - 5, counter_ind]) cube([diode_block_dx, diode_block_dx, form_height]);
        // - diode body
        color("fuchsia") translate([lead_ox - lead_indent, v_1 - diode_l - diode_offset, form_height - ind - lead_indent]) cube([diode_d, diode_l, diode_d + extra_indent + 1]);
    }
    // lead block
    translate([diode_block_dx, y0 - lead_block_dy/2 - lead_d/2, 0]) difference() {
        w = lead_connection_length - (diode_block_dx - lead_d)/2;
        lead_oy = (lead_block_dy - lead_d)/2;
        cube([w, lead_block_dy, form_height]);
        // middle lead
        color("red") translate([-1, lead_oy, form_height - ind])  cube([w + 2, lead_d, ind + 1]);
        // square cut-away
        color("turquoise") translate([0, lead_oy + lead_d, form_height - ind] )rotate(-45, [0,0,1]) translate([-10, 0, 0]) cube([10, 10, form_height]);
    }
}

module diode_bender_left() {
    // base block
    color("green") cube([30, depth, 3]);
    translate([0,0,3]) form_block();
}

diode_bender_left();
