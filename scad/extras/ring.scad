height = 2.5;
inner_d = 7.2;
outer_d = 10.2;

difference() {
    cylinder(d=outer_d, h=height, $fn=45);
    translate([0,0,-1]) color("yellow")
        cylinder(d=inner_d, h=height+2, $fn=45);
}
