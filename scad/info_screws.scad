use <src/terminal.scad>
use <src/thumb_iface.scad>
use <src/thumb-cube.scad>

echo ("main case screws: ", screw_length());
echo ("thumb connector screws: ", thumb_connector_screw_length());
echo ("thumb case screws: ", thumb_case_screw_length());
echo ("tray connection screws: ", tray_brim_screw_length());
echo ("controller mount screw: ", controller_mount_screw_length());
