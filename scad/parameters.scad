key_width = 18;
switch_width=16;
switch_indent=0; //0.8;
border=3;  // extra space around the switches to the case end
plate_thickness = 3;
bottom_thickness = 2;
wall = 2; // thickness of all walls
wall_nudge_width = 1;
wall_nudge_height = 1.5;        // how much the bottom case grabs over the plate
wall_nudge_w_clearance = 0.1;   // width clearance is taken away from the plate 
wall_nudge_h_clearance = 0.1;   // height clearance is taken away from the case
column_offsets = [0,0,4,8,4,4,4];

inner_height = 9;

encoder_hole_d = 7.40;

jack_diameter = 8.8;
jack_front_diameter = 6.4;

// -- thumb bone ------------------
key_circle_r = 92;
thumb_circle_center = [4*key_width + border + 3, -key_circle_r + 2];
thumb_sphere_d = 350;
// --------------------------------
//

// -- thumb cube ------------------
tc_inner_height = 15;
// --------------------------------


use_led_diffusor = false;


// -- screw parameters ------------
screw_bolt_d = 8;   // diameter of the plstic bolt holding a screw
screw_bolt_fn = 50;
screw_head_a = 90;  // angle, DIN 963
//m2
//screw_d = 2 + 0.2;        // bolt diameter, 2 is the DIN 963 value
//screw_head_d = 3.8 + 0.2; // head diameter, 3.8 is the DIN 963 value
//m3
screw_d = 3 + 0.4;        // bolt diameter, 3 is the DIN 963 value
screw_head_d = 5.6 + 0.8; // head diameter, 5.6 is the DIN 963 value
// --------------------------------


// -- screw parameters ------------
// m2
//nut_size=4;         // DIN 934
//nut_height=1.6;     // DIN 934
// m3
nut_size=5.5 + 0.2;        // DIN 934
nut_height=2.4 + 0.2;      // DIN 934
// --------------------------------


plug_plate_thickness = 4;
plug_plate_height = 16;

c_plate_fixator_extra_width = 3;
c_plate_fixator_thickness = 3;


case_connection_die_length = 50;

low_profile_screw_points = [
    // left side, bottom to top
    [0, screw_bolt_d/2],
    [0, 2.5*key_width + border],
    [0, 5*key_width + 2*border - screw_bolt_d/2],
    // top row, left to right (without left most)
    [2*key_width, 5*key_width + 2*border],
    [3*key_width, 5*key_width + 2*border + column_offsets[2]],
    [4*key_width + 2*border + 3, 5*key_width + 2*border + column_offsets[4]],       // promic front left
    [7*key_width + 0*border - 3, 5*key_width + 2*border + column_offsets[4]],       // promic front right
    [7*key_width + 2*border, 5*key_width + 2*border + column_offsets[4]], 
    // bottom row, left to right (without left most)
    [2*key_width + 2*border, screw_bolt_d/2],
    [3*key_width + 2*border, screw_bolt_d/2 + column_offsets[2]],
    [4*key_width + 2*border - screw_bolt_d/2, screw_bolt_d/2 + column_offsets[2]],  //thumb 1 (from left to right)
    [4*key_width + 2*border + screw_bolt_d/2, 1*key_width + column_offsets[4]],     //thumb 2
    [6*key_width + 1*border, 1*key_width + column_offsets[4]],                      //thumb 3
    [7*key_width + 2*border, 1*key_width + screw_bolt_d/2 + column_offsets[4]],     //thumb 4 (right most)
    // right, middle
    [7*key_width + 2*border, 3*key_width + 1*border + column_offsets[4]], 

];

screws_w_c_tray_connection = [
    // [ screw index, has nut]
    [5, true],
    [6, true],
    [11, false],
    [13, false],
    [14, true]
];

screws_w_case_nut = [
    0,1,2,3,4,7,8,9
];

thumb_screw_connections = [
    // [ screw index,   direction (0:right, 1:down)  ]
    [10,  0],
    [11,  1],
    [12,  1],
    [13, 1]
];


// -- controller mount plate ------
c_plate_width = 24;
c_plate_distance = 8; // distance between controller plate and the finger plate
c_plate_thickness = 1.5;
c_plate_length = 42;
c_mount_plate_thickness = 1.5;
c_mount_screw_elevation = 1.8;  // distance from the nut to the bottom (= thickness of the bottom plate beneath the nut)
c_mount_nut_z_clearance = 0.8;  // extra space above/beneath the nut; not too small because of overhead stringing
// the clearance goes “down”, so the elevantion must be bigger or equal than the clearance (equal means that there is no bottom at all)
// inner_height + bottom + tray_height = c_plate_distance + c_plate_thickness + plug_plate_height
c_tray_inner_height = c_plate_distance + plug_plate_height - inner_height - bottom_thickness + 0.3 - 1; 
c_tray_lower_width = 37;
c_tray_upper_width = 30;
c_tray_base_height = 3;
c_tray_length = case_connection_die_length + 20;
c_tray_upper_height = c_tray_inner_height - c_tray_base_height + bottom_thickness;
c_tray_brim_thickness = 2;
c_tray_brim_con_height_w_nut = 4;
c_tray_brim_con_height_wo_nut = 1.2;
// --------------------------------
//

// -----Thumb Interface------------
tc_middle_cylinder_height = 5;
tc_middle_cylinder_offset = -plate_thickness - tc_middle_cylinder_height / 2 - (bottom_thickness + inner_height)/2;
tc_middle_cylinder_clearance = 0.4;
tc_lower_cylinder_height = 8;
tc_tray_clearance = 0.2;
// --------------------------------


support_screw_hole_depth = 9.5;
support_screw_points = [
    // [point of lower left - border-(key_with-switch_width)/2], stake_n?, stake_e?, stake_s?, stake_w?, height
    [
        [1*key_width,  2*key_width],
        true, true, true, true,
        inner_height
    ],
    [
        [2*key_width,  3*key_width + column_offsets[2]],
        true, true, true, false,
        inner_height
    ],
    [
        [3*key_width,  2*key_width + column_offsets[3]],
        true, true, true, false,
        inner_height
    ],
    [
        [4*key_width,  3*key_width + column_offsets[3]],
        true, false, true, true,
        inner_height
    ],
    [
        [5*key_width,  3*key_width + column_offsets[5]],
        true, true, true, true,
        c_plate_distance
    ],
    [
        [6*key_width,  3*key_width + column_offsets[6]],
        true, true, true, true,
        c_plate_distance
    ],
];
support_screw_bolt_stake = 3;

promic_length = 33.7; // must be in sync with value in promicro_mount.scad
promic_front_notch = 1.5; // must be in sync with front_notch_depth in promicro_mount.scad

switch_offset = [
    (key_width - switch_width)/2,
    (key_width - switch_width)/2,
    0
];


controller_front_center_xy = [
    border + 5.5*key_width,
    2*border + 5*key_width + column_offsets[3]
];


controller_part_anchor_xy = [
    controller_front_center_xy.x,
    controller_front_center_xy.y - wall - plug_plate_thickness

//    2*border + 5*key_width + column_offsets[6] - plug_plate_thickness/2 - wall/2 , 
];


controller_part_anchor = [
    controller_part_anchor_xy.x,
    controller_part_anchor_xy.y,
    -plate_thickness - c_plate_thickness - c_plate_distance
];


controller_mount_screw_xy = [
    controller_part_anchor.x,
    controller_part_anchor.y - c_plate_length + screw_bolt_d/2
];


function case_bottom_z() = -(plate_thickness + bottom_thickness + inner_height); 
function screw_length() = -case_bottom_z();
function tray_brim_screw_length() = -case_bottom_z() + c_tray_brim_con_height_w_nut;
function c_tray_inner_height() = c_tray_inner_height;
