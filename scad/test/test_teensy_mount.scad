use <../src/lib/teensy_mount.scad>

difference() {
    translate([-4, -17, -1.4]) {
        cube([40,34,1.4]);
        cube([4, 34, 12]);
    }
    teensy_front_cutaway();
}
teensy_mount();
