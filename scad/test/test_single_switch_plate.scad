use <../src/lib/cherrymx_switch.scad>

plate_length = 25;


module plate() {
    linear_extrude(3) {
        square(plate_length,plate_length);
    }
}

module switch_hole_plate() {
    difference() {
        plate();
        translate([
            plate_length/2 - 16/2,
            plate_length/2 - 16/2,
            3 
        ]) switch_hole();
    }
}

rotate(180, [0,1,0])
    switch_hole_plate();
