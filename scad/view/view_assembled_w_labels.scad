use <../src/terminal.scad>
use <../src/thumb.scad>

finger_plate();
thumb_plate();
outer_case();
controller_part();
controller_mount_part();
//controller_tray();

v_labels();
h_labels();

echo("<font color='blue'><b>Screw length:", screw_length(), "mm</b></font>");
