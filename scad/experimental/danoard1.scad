use <cherrymx_switch.scad>
use <plate_bolt.scad>
use <teensy_mount.scad>


key_width = 18;
switch_width=16;
border=5;
plate_thickness = 3;
wall = 2;
column_offsets = [0,0,5,8,4,4,4];
cutaway_circle_r=90;
thumb_circle_center = [4*key_width + border, -cutaway_circle_r + 7];


module case_outline() {
    difference() {
        union() {
            color("green")
                square([7*key_width + 2*border, 5*key_width + 2* border + 5]);
            color("blue")
                translate([100,0,0])
                    rotate(-39) square([48,45]);
            color("blue")
                translate([7*key_width + 2*border, 5*key_width + 2* border + 5 -75])
                    square([30,75]);
        }
        color("red")
            translate(thumb_circle_center)
                circle(r=cutaway_circle_r, $fn=100);
        color("red") translate([155,-20]) square([40,40]);
    }

}

module case_solid() {
    inner_height = 15;
    outer_height = 15;
    difference() {
        color("green") 
            translate([0, 0, -inner_height]) linear_extrude(inner_height) case_outline();

    
        color("red")
            translate([-200, -50, -inner_height - 50])
                translate([border + 6*key_width, 0, inner_height - outer_height])
                cube([200,200,50]);
    }
}


module thumb_switches() {
    key_circle_r = 92;
    translate(thumb_circle_center)
        for (degrees = [-6, -17, -28, -39]) {
            rotate(degrees, [0,0,1])
                translate([-switch_width/2, key_circle_r, 0])
                    switch_hole();
        }
}


module finger_switches(thumb_col=false) {
    switch_offset = [
        (key_width - switch_width)/2,
        (key_width - switch_width)/2,
        0
    ];

    translate([border, border, 0])
        for (i = [0:len(column_offsets)-1]) {
            a = i > 3 ? 1 : 0;
            translate([i*key_width, column_offsets[i],0]) 
                for (i=[a:4]) {
                    translate(switch_offset)
                        translate([0, i*key_width, 0])
                            switch_hole();
                }
        }
}


module case_upper_base_cut() {
    nudge_wall = 1;
    bottom_plate_thickness = 2;
    height = 15; // assuming only one height; to be global and = inner_height = outer_height
    switch_indent = 0.8;
    difference() {
        case_solid();
        translate([0,0, -plate_thickness])
            translate([0,0,-50]) linear_extrude(50) offset(-wall) case_outline();
        translate([0,0, -height + bottom_plate_thickness])
            translate([0,0,-50]) linear_extrude(50) offset(-nudge_wall) case_outline();
        translate([0,0,-switch_indent]) {
            thumb_switches();
            finger_switches();
        }
    }

}

module case_upper() {
    difference() {
        union() {
            case_upper_base_cut();
            translate([149, 105 - wall, -plate_thickness])
                rotate(180, [0,1,0]) rotate(270)
                    teensy_mount();
        }
        translate([149, 105 - wall, -plate_thickness])
            rotate(180, [0,1,0]) rotate(270)
                teensy_front_cutaway();
    }
}

case_upper();

//translate([border, border, 0]) left_board();
