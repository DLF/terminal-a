
module test1() {
    rotate(90, [1,0,0]) {
        for (i=[0:30]) {
            translate([i*0.4*0.9, 0, 0])
            cylinder(d=0.4, h=30, $fn=18);
        }
        for (i=[0:30]) {
            translate([i*0.4*0.9 + 0.2, 0.4*0.6, 0])
            cylinder(d=0.4, h=30, $fn=18);
        }
    }
}

module test2() {
    cube([100,40, 0.8]);
}

module test3(){
    for(i=[0:30]) {
        translate([i*1.2,0,0])
        rotate(45,[0,1,0]) cube([1, 40, 1]);
    }
}

module test4(){
    cube([31*1.2, 40, 0.2]);
    for(i=[0:30]) {
       translate([i*1.2,0,sqrt(2)/2])
        rotate(45,[0,1,0]) cube([1, 40, 1]);
    }
}

module test5(){
    bridge_w = 0.6;
    channel_w = 0.6;
    pattern_w = bridge_w + channel_w;
    channel_h = 0.6;
    cyl_d = 1;
    xn = round(45 / pattern_w);
    cube([xn*pattern_w, 40, 0.2]);
    for(i=[0:xn-1]) {
        translate([i*pattern_w, 0, 0.2])
            cube([bridge_w, 40, channel_h]);
    }
    translate([0, 0, 0.2+channel_h]) cube([xn*pattern_w, 40, 0.2]);
    for(i=[1:xn-1]) {
        translate([i*pattern_w + bridge_w/2, 0, 2*0.2 + channel_h - 0.1])
            rotate(270, [1,0,0])
                cylinder(d=cyl_d, h=40, $fn=15);
    }

}

module test6(){
    bridge_w = 0.6;
    channel_w = 0.6;
    pattern_w = bridge_w + channel_w;
    channel_h = 0.6;
    depth = 45;
    length = 45;
    x_bridge_distance = 14;
    xn = round(length / pattern_w);
    yn = round(depth / x_bridge_distance);
    color("green") cube([xn*pattern_w, depth, 0.2]);
    for(i=[0:xn-1]) {
        translate([i*pattern_w, 0, 0.2])
            cube([bridge_w, depth, channel_h]);
    }
    color("green") translate([0, 0, 0.2+channel_h]) cube([xn*pattern_w, depth, 0.2]);
    for(i=[0:xn-1]) {
        translate([i*pattern_w + pattern_w/2, 0, 2*0.2 + channel_h])
            cube([bridge_w, depth, channel_h]);
    }
    for(i=[0:yn]) {
        translate([0,i*x_bridge_distance, 2*0.2 + channel_h])
            cube([length, bridge_w, channel_h]);
    }
    color("green") translate([0, 0, 2*(0.2+channel_h)]) cube([xn*pattern_w, depth, 0.2]);
}

test6();
