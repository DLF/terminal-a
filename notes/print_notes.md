No brim!!!!

## Key Caps
* Printing upright. (Stamp down, finger surface up.)
* Layer hight: 0.1 mm max (for a proper finger surface; otherwise 0.2 mm also does the job)
* Line width: 0.2 mm max.
* Support highly recommended.

  Support X/Y distance must be larger than the stamp cross (0.8 mm work in Cura).
  Otherwise, we get a awful support strain within the stamp cross.

  Standard support is preffered over tree support, a low support density is recommended. (~8% to 10% in Cura)
* Avoid adhesion support and take care for good natural adhesion on the bed.
  Removing adhesion support later for each cap is a lot of effort.

  If adhesion support is needed, avoid in any case that the switch stamp cut away (the cross) is covered.


## Cases (Thumb Cube and Main Case)
* Use Tree Support (no restriction needed; with M3 screws, also the nut pits can be printed with support)
  
  It might be a good idea to use a low support density; removing the support cleanly is hard but crucial.
* Layer height and line width should be 2mm. More precision is not needed, but less might cause trouble for screw holes, nut pits, support screw indents and the thumb-main-case interface.

### Settings from 5ff0956 main case test print
* 2mm / 2mm
* Tree Support, Support Density 10%, no limit (also for the nut pits)
* 4mm Brim
* Temp: 230 / 65 (White PLA)
* 25% infill (is fine)

## Finger Plates (Thumb Cube and Main Plate)
* No support!
* Layer height and line width: 0.2 mm max)
* Try with Brim next time to avoid edge folding

### Outdated since switch indent removed
Biggest challenge here is to get the overhang of the switch mount holes fine.
Cleaning those after print might work with a die grinder.
This is even more important since any stringing at the switch mount holes will make the switches sitting skewed in the case later on.
This is a critical part of the print.

### Settings from 5ff0956 test print
* 2mm / 2mm
* Infill 50% / Grid (less infill again next time)
* No support, no adhesion support
* Top/Bottom/Wall-Layers: 3
* Temp: 220°C / 65°C
* Filament: White PLA “Das Filament”

→ Adhesion prob: folding up at corners (to cold?)

## Controller Mount Part
* Layer height and line width: 0.2 mm
* No support
* Post-Processing: Drilling the screw hole might be necessary due to overhand stringing over the nut pit.

## Controller Tray
* Layer height and line width: 0.2 mm
* Support needed. Using standard support needs easily 1/10th less time compared to tree support. For removal, a driller might be needed for the jack mount hole.

## Controller Plate
* Like a case, but a much higher infill is recommended to make the plug-plate properly stable.
