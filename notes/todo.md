✓ rearrange encoder mount
✓ add hole for reset switch to tray
✓ more clearance for thumb iface
✓ clearance for controller plate socket (finger plate part)
✓ enlarge screw head (so that the screw goes a bit deeper); maybe the plate thickness has to be slightly increased
✓ rot knob: add finger pit on the bottom
✓ rot knob: increase grip pits massively
✓ rot knob: enhance the fit on the rot encoder
→ decrease the depth of support screw head sinks slightly (a tiny bit more for v2)
• mx switch hole must fit better for gaterons: they easly slip out → to experiment
• mx keycap: cross clearance configurable
• thumb case: most inner screw bold can be fully connected to the case (also on the lower side, not only on the outer)
• tray: make Teensy reset hole optional and deactivate it
• place a reset button somewhere somehow
• tray: check if it makes sense to remove the bottom of the tray-thumb connection hole
• check if the tray-case connection hole can be a bit wider — it would help to place the LED and serial cables beside the controller plate
• add usb cable socket hole in main case
• Caps: add tactile marker for F and J
• Caps: shrink cap surface; distance between cap-tips must be greater
• Maybe: Caps: row 1 a bit highter (.5 mm?); first check after shrinking cap surgace
• Caps: row 3 lower: starting at same higth as row 2

