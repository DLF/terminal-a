Terminal A
==========

A DIY, QMK based, Pro Micro driven, 3D-printable split keyboard.

This repository hosts the 3D-printable parts.

My QMK fork resides [in a GitHub repository](https://github.com/DLFW/qmk_firmware)
with having all “Terminal A” changes on the 
[terminal-a branch](https://github.com/DLFW/qmk_firmware/tree/terminal-a).

Overview
--------

“Terminal A” is my first self-designed keyboard.
It’s a split keyboard, each side with a 7 × 5 column-staggered key-plate,
four additional thumb keys, and one rotary encoder with an additional switch.
The switches have a slightly smaller spacing than standard boards,
the key-caps are also 3D-printed and part of the project.

It furthermore features RGB lighting with different colors for each layer.
The color of each layer can be adjusted in hue, saturation and value
with the board itself.
Changes are persistently stored in EEPROM.

One design goal was easy “printability” and the use of solely simple, standard parts.

I’m quite happy with the result but not fully satisfied.
While it’s easy to print, it needs some patient fingers to wire everything.
I would also liked it to have a more stable standing.
It needs some well-placed rubber pads.
However, I’m happily using it since several months now.

Some Impressions
----------------

![Prototype](doc/img/terminal-a-front.jpg)
![Prototype](doc/img/proto_1.jpg)
![Prototype](doc/img/collage.jpg)



